const express = require('express');
const app = express();

//Rotas
const index = require('./routes/index');

//Para requisição com json (limite de 50mb)
app.use(express.json({limit: '50mb', extended: true}));
//Para requisição url (limite de 50mb)
app.use(express.urlencoded({limit: '50mb', extended: true}));

//Utilização das rotas
app.use('/', index);

module.exports = app;