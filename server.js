//Calls app.js
const app = require('./app');

const port = 8081;

//Start server
app.listen(port, () => {
  console.log("Server running on port " + port);
 });
