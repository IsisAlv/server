# README #

Espero que este README ajude a deixar tudo funcionando :)

### O que é este repositório? ###

Este repositório é um exemplo de um servidor em Node.js bem simples, para começar!

### Setup ###

* Baixe o conteúdo ou clone o repositório com o Git
* Na pasta do projeto, rode o comando de instalação das dependências:
		
		npm i
				
* Rode o comando para iniciar o servidor:

		node server.js

* E tá pronto o sorvetinho!